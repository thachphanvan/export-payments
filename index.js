const fs = require('fs');
const path = require('path');
const myRequest = require('./myRequest.js');
const sleep = require("./sleep");

// const HORIZON_URL="https://horizon.triamnetwork.com";
const HORIZON_URL="http://horizon564cd0909fa7d3b2.triamnetwork.com";
const exportPaymentHistory = async (address, tokenName, logger) => {
    logger.write(`txId , from , to, amount \n`);
    let url = `${HORIZON_URL}/accounts/${address}/payments?limit=200`;
    let result = await myRequest(url);
    let payments = result && result._embedded && result._embedded.records || [];
    let loop = payments.length > 0;
    while(loop){
        let onlyTransferWGXPayments = payments.filter(payment => {
            return payment.type === "payment" && payment.asset_code === tokenName;
        });
        onlyTransferWGXPayments.forEach(op => {
            console.log(`${op.transaction_hash} ,${op.from} ,${op.to}, ${op.amount}`);
            logger.write(`${op.transaction_hash} ,${op.from} ,${op.to}, ${op.amount}\n`);
        });
        // get the payments on next page
        url = result && result._links.next.href;
        console.log(url);
        await sleep(2000);
        result = await myRequest(url);
        payments = result && result._embedded && result._embedded.records || [];
        loop = payments.length > 0;
    }
}
const MY_ADDRESS="GCSJMVRD43JLFR7GXG7EESMFVB53BHGODTZ2EY7C63I7EGW3JWAG2F6L";
const OUTPUT_NAME=`output_paymentWGX_${MY_ADDRESS.substring(0,4)}_${Date.now()}.csv`;
const logger = fs.createWriteStream(path.resolve(__dirname,`${OUTPUT_NAME}`), {
    encoding: 'UTF-8'
});

exportPaymentHistory(MY_ADDRESS,"WGX", logger).catch(console.log);
