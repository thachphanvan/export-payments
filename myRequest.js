const request = require('request');

const myRequest = uri => {
    return new Promise((resolve, reject) => {
        request({
            method: "GET",
            uri,
            json: true,
            headers: {
                "Content-Type": "application/json"
            }
        },
            function (error, response, body) {
                if (error) return reject(error);
                if (response.body.errors) {
                    return reject(errors);
                }
                resolve(response.body);
            });
    })
}

module.exports = myRequest;
