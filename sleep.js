
const sleep = (timeOut) => {
    return new Promise((resolve, reject) => {
        setTimeout(resolve, timeOut);
    });
}
 module.exports = sleep;
