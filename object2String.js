const objectToCSVString = (obj) => {
    if(!obj || typeof obj === 'string'){
        return obj || '';
    }

    let result = '';
    for(let key in obj){
     result += `${key}: ${obj[key]} | `;
   }
   return result;
}

module.exports = objectToCSVString;
